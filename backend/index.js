var express = require("express");
var bodyparser = require("body-parser");
var fs = require("fs").promises;
var fsExists = require("fs.promises.exists");
var path = require("path");
var md5 = require("md5");
const { mainModule } = require("process");

const app = express();
const port = 8080;
const storage_dir = "../collected_data/"

var table = {}

app.use(bodyparser.json());
app.use(express.static("../frontend/build"));

app.get("/api/data", function(req, res)
{
    res.send(table);
});

app.post("/api/data", async function(req, res)
{
    try
    {
        var tableString = tableToString(req.body);
    }
    catch (e)
    {
        return res.status(400).send("Invalid request body");
    }

    let filePath = await createFilePath(tableString);

    await fs.writeFile(filePath, tableString);

    res.send();
});

function tableToString(table)
{
    return table.map((s) => s.join(",")).join("\n");
}

async function createFilePath(fileContents)
{
    let filename = md5(fileContents)+".csv";
    let filePath = path.join(storage_dir, filename);

    let fileExists = await fsExists(filePath);
    
    if (fileExists)
    {
        return await createFilePath(filePath);
    }
    else
    {
        return filePath;
    }
}

async function main()
{
    contents = await fs.readFile("./data.csv", "utf8");
    table = csvToTable(contents);

    app.listen(port, () =>
    {
        console.log(`Listening on port ${port}`);
    });
}

function csvToTable(csv)
{
    let parsedTable = {};

    let lines = csv.split(/\r?\n/);
    let headers = CSVtoArray(lines[0]);

    for (let i = 0; i < headers.length; i++)
    {
        parsedTable[headers[i]] = [];
    }

    for (let i = 1; i < lines.length; i++)
    {
        let currentStr = lines[i];
        let line = CSVtoArray(currentStr);

        for (let j = 0; j < line.length; j++)
        {
            parsedTable[headers[j]].push(line[j].trim().replace("\"", ""));
        }
    }

    return parsedTable;
}

//Functiion from (https://stackoverflow.com/questions/8493195/how-can-i-parse-a-csv-string-with-javascript-which-contains-comma-in-data)
// Return array of string values, or NULL if CSV string not well formed.
function CSVtoArray(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    if (!re_valid.test(text)) return null;
    var a = [];                     // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

main();