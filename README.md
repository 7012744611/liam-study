The first time you run this, double click setup.sh. You only have to run it once.

To run the application, double click "run.sh".
When it says "Listening on port 8080" in the terminal, type localhost:8080 into the address bar of your browser to
see the page.

Consent form to replace is frontend/build/consent_form.pdf
Data csv to replace is backend/data.csv

On submission the csv file for the user's table gets placed in collected_data 

If you replace the data file, restart the app.
If it fails to sumbit for some reason you can restart the server, as long as you don't close out the web page it'll be fine.

I added some post processing to remove the commas from numbers on submission to save you a headache later. As a side effect, don't include commas in any string columns or it will explode. Always test the thing before using it.