import './App.css';
import React from 'react';

import {
	BrowserRouter as Router,
	Route,
	Routes,
} from "react-router-dom";
import SortingPage from './Pages/SortingPage';
import ConsentPage from './Pages/ConsentPage';
import SubmissionOverlay from './ui-components/SubmissionOverlay';

class App extends React.Component
{
	render() 
	{
		return (
			<div className="App">
				<header className="App-header">
				<div className="App">
					<Router>
						<Routes>
							<Route path="/loading" element = {<SubmissionOverlay visible={true} success={true} />} />
							<Route path="/" element={<ConsentPage />} />
							<Route path="/sort" element={<SortingPage />} />
						</Routes>
					</Router>
				</div>
				</header>
			</div>
		);
	}
}

export default App;