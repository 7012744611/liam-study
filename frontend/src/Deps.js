import DataService from "./Services/DataService"
import TableRandomizer from "./Services/TableRandomizer"

export let TableStorage = new DataService();
export let TableShuffle = new TableRandomizer();