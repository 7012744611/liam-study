import React from "react"
import {List} from "react-movable"
import {TableStorage, TableShuffle} from "../Deps";

const RANK_HEADER_TITLE = "#";

export default class Table extends React.Component
{
	constructor(props)
	{
		super(props);

		this.table = null;
		this.headers = []
		this.rows = [];
		this.initialized = false;

		TableStorage.GetTable()
		.then(data =>
		{
			let table = TableShuffle.randomize(data);
			
			this.table = table;
			this.headers = table.headers;
			this.rows = table.rows;
			this.initialized = true;

			this.forceUpdate();
		});
	}

	getData()
	{
		if (!this.initialized)
		{
			throw new Error("Cannot get data from uninitialized table");
		}

		return this.table;
	}

	render()
	{
		if (this.initialized)
		{
			return (
				<List values={this.rows}
					onChange={({ oldIndex, newIndex }) =>
					{
						let direction = Math.sign(newIndex - oldIndex);
						for (let i = oldIndex; i != newIndex; i += direction)
						{
							let temp = this.rows[i];
							this.rows[i] = this.rows[i+direction];
							this.rows[i+direction] = temp;
						}
					}
					}
					renderList={testTable(this.headers)}
					renderItem={testItem()}
				/>
			)
		}
		else
		{
			return (<h1>Loading...</h1>)
		}
	}
}

function testTable(headers)
{
	return function({children, props})
	{
		return (
			<table className="table">
				<thead>
					<tr>
						<th scope="col">
							{RANK_HEADER_TITLE}
						</th>
						{headers.map(h => (
							<th scope="col">{h}</th>
						))}
					</tr>
				</thead>
				<tbody {...props}>
					{children}
				</tbody>
			</table>
		);
	}
	
}

function testItem()
{
	return function({value: row, props})
	{
		return (
			<tr {...props}>
				<th scope="row">⬍ {props.key != null? props.key+1 : ""}</th>
				{/* <td>
					<div classame="pad-horizontal pad-vertical">⬍ {props.key != null? props.key+1 : ""}</div>
				</td> */}
				{row.map(v =>
					<td>
						<div classame="horizonal-margin vertical-margin">
							{v}
						</div>
					</td>
				)}
			</tr>
		);
	}
}