
export default (props) =>
{
    return (
        <div
            style={{
                position: "absolute",
                top: "0px",
                left: "0px",
                minWidth: "100vw",
                minHeight: "100vh",
                backgroundColor: "rgba(0,0,0,0.5)",
                zIndex: 10,
                alignItems: "center",
                justifyContent: "center",
                textAlign: "center",

                display: props.visible? "flex" : "none",
            }}
        >
            <div style={{display: props.success? "none" : "block"}}>
                <div className="spinner-border text-info" role="status">
                    <span className="sr-only"></span>
                </div>
                <div className="text-info">
                    Submitting...
                </div>
            </div>

            <div style={{display: props.success? "block" : "none"}}>
                <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="lawngreen" class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                </svg>
                <h1 style={{color: "lawngreen"}}>
                    Done
                </h1>
            </div>
            
        </div>
    );
}