import ShuffledTable from "../objects/ShuffledTable";

class TableRandomizer
{
    randomize(table)
    {
        let rows = [];
        let headers = Object.keys(table);
        
        shuffle(headers);

        for (let i = 0; i < headers.length; i++)
        {
            shuffle(table[headers[i]]);
        }

        let tableHeight = table[headers[0]].length;

        for (let y = 0; y < tableHeight; y++)
        {
            let row = [];

            for (let x = 0; x < headers.length; x++)
            {
                row.push(table[headers[x]][y]);
            }

            rows.push(row);
        }

        return new ShuffledTable(headers, rows);
    }
}

function shuffle(array)
{
	for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

export default TableRandomizer;