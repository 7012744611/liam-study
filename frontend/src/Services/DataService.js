import $ from "jquery";

class DataService
{
    async GetTable()
    {
        return await $.get("/api/data");
    }

    async SubmitTable(table)
    {
        return await $.ajax({
            url: "/api/data",
            type: "post",
            data: JSON.stringify(table),
            contentType: "application/json; charset=utf-8",
            processData: false,
        });
    }
}

export default DataService;