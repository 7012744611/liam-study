import React from "react";
import { useNavigate } from "react-router-dom";

export default (props) =>
{
    var nav = useNavigate();
    return (<ConsentPage {...props} redirect={nav} />)
}

class ConsentPage extends React.Component
{
    constructor(props)
    {
        super(props)

        this.form = React.createRef();
        this.checkbox = React.createRef();
    }

    submit()
    {
        if (this.checkbox.current.checked)
        {
            this.props.redirect("/sort");
        }
        else
        {
            alert("You must check 'I Agree' to proceed");
        }
    }

    render()
    {
        return (
            <div style={{display: "flex", flexDirection: "column", minWidth: "80vw", minHeight: "98vh"}}>
                <div style={{display: "flex", flex: 1}}>
                    <iframe ref={this.form} src="consent_form.pdf" style={{width: "100%"}}>
                    </iframe>
                </div>

                <div className="form-check">
                    <input
                        class="form-check-input"
                        type="checkbox"
                        ref={this.checkbox}
                        id="consent"
                        style={{float: "none", marginRight: ".5em"}}
                    />
                    <label className="form-check-label" for="consent">
                        I Agree
                    </label>
                </div>
                
                <div style={{margin: "10px"}}>
                    <button
                        onClick={() => this.submit()}
                        className="btn btn-primary"
                        style={{width: "150px"}}
                    >
                            Submit
                    </button>
                </div>
            </div>
        );
    }
}