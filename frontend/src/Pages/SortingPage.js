
import React from "react";
import Table from "../ui-components/Table";
import {TableStorage} from "../Deps"
import { useNavigate } from "react-router-dom";
import SubmissionOverlay from "../ui-components/SubmissionOverlay";


const SUBMISSION_REDIRECT_DELAY = 2;

export default (props) =>
{
    var nav = useNavigate();
    return (<SortingPage {...props} redirect={nav}/>)
}

class SortingPage extends React.Component
{
    constructor(props)
    {
        super(props);
        
        this.table = React.createRef();

        this.state = {
            submitting: false,
            success: false,
        };
    }

    async submit()
    {
        let confirmSubmit = window.confirm("Press 'OK' to confirm submission.");

        if (confirmSubmit)
        {
            try
            {
                this.setState({submitting: true});

                let table = this.table.current.getData()
                await TableStorage.SubmitTable(table.formatForSubmission());

                this.setState({success: true});
                await new Promise((resolve) => setTimeout(resolve, SUBMISSION_REDIRECT_DELAY*1000));
                this.props.redirect("/");
            }
            catch (e)
            {
                this.setState({submitting: false});
                alert("Error: Failed to submit");
                console.log(e);
            }
        }
    }

    render()
    {
        return (
            <div>
                <SubmissionOverlay visible={this.state.submitting} success={this.state.success}/>
                <div>
                    <Table ref={this.table} />
                </div>
                <div>
                    <button
                        onClick={() => this.submit()}
                        className="btn btn-primary"
                        style={{width: "150px"}}
                    >
                            Submit
                    </button>
                </div>
            </div>
        )
    }
}