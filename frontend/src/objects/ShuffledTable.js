
export default class ShuffledTable
{
    constructor(headers, rows)
    {
        this.headers = headers;
        this.rows = rows;
    }

    formatForSubmission()
    {
        let submissionData = [];

        submissionData.push(this.headers);
        for (let i = 0; i < this.rows.length; i++)
        {
            let submissionRow = [];
            let currentRow = this.rows[i];

            for (let j = 0; j < currentRow.length; j++)
            {
                let currentCell = currentRow[j];
                submissionRow.push(
                    typeof(currentCell) === 'string' && currentCell.includes(",")?
                        currentCell.replaceAll(",", "")
                        :currentCell
                );
            }

            submissionData.push(submissionRow);
        }

        return submissionData;
    }
}